# Password Shield

## FAQ

- **How do I set a secure password?**

    The strength of the password shield is directly proportional to the strength of the password you set.  There is no single solution to building a strong password, however there are some general guidelines that can help "stack the deck" in your favor.

        - Longer is better than shorter.
        - Dictionary words should be avoided.
        - Randomness is helpful.
        - Try to avoid known hacked passwords.

    **For a lengthy discussion regarding how password strength is derived please read our long form article on passwords.**

- **Is the password I set used as the encryption key?**

    No.

    ShieldMyfiles uses [One-Time-Pad](./Shielding-Files.md) (OTP) ciphers to shield files. OTP operates by issuing a pad (or key) that is equal in size to the file you are protecting, and then combining your file with the corresponding character in the pad. As such, the password you set could not possibly the key (pad) used to encipher your file(s). The password you set is used by ShieldMyfiles as the mechanism by which we authorize release of the pad to your intended recipient.

- **How does the person opening my file know the password I set?**

    The password you set for your shield should be a shared secret between you and your intended recipient. It is your responsibility to ensure that your intended recipient knows the password, ShieldMyfiles is unable to provide that information on your behalf.  ShieldMyfiles operates in this manner to make good on our commitment to provide you protection without possession.

    - **So how do I get the password to my intended recipient?**

        We recommend two strategies:

        1. Use a known shared secret. For example, the Shielder can use any piece of shared knowledge to serve as the password. Ideally, this password will not be sensitive personal information, such as a social security number. An example of this could be "your favorite baseball team". This method ensures that the password is understood without being disclosed.

        2. Convey the password to the intended recipient through a different communication channel.  For example, if you email a shielded file to your intended recipient, send the password via SMS text. This method is not perfect for those that are concerned that all of their communications are monitored, but "multi-channel" communications will lessen the risk of exposure.

    - **There are other shields that do not require a password or other information to be known or shared ahead of time.**

        ShieldMyfiles is currently developing a series of shields that do not require a shared secret. When these shields are ready you can use them to send shielded files to recipients without having to use a shared secret, or conveying a password to your recipient though an alternative channel.

        - SMS Text Shield - enter your recipient's cell number and we will text them the access code.
        - Email Shield - enter your recipient's email address and we will email them the access code.

- **What does "I don't want the password to be resettable (most secure)" mean?**

    By default, ShieldMyfiles password Shields are "resettable".

    This means that if you need to change your password at some point after you have Shielded your file, you can do so without having to open the original protected file and and then repeat the protection process a second time.  This feature is useful if you wish to change the password to a Shielded file down the road, or if you simply forgot the password.

    Increased Security:

    The resettable feature is an optional toggle because by turning it off, you can enhance the security of your Shielded file.

    By eliminating the option to reset, you never need to worry about an unauthorized party changing the password without your permission or knowledge.

    *By that same token however, if you forget the password, that file is __irretrievable by you and by ShieldMyfiles.__*

- **What if I forget my password?**

    Fear not! The ShieldMyfiles password Shield is resettable by default, meaning that if you forgot your password, you can reset it using the verified email tied to your account.

    If, you elected to turn off the reset feature on the password Shield, your password is irrecoverable. The enhanced security that is created by making your password non-resettable comes at the cost of the file being irretrievable if the password is forgotten.

- **Why can't I use more than one password Shield?**

    The simple answer is that multiple iterations of the same shield do not enhance your security, but they greatly increase the difficulty of accessing your files.

    As mentioned above, the Shield password used to protect your files is separate and distinct from the cryptographic pad we use to encipher your data. As such, adding shields does not increase cryptographic strength, or provide increased randomness. Adding that extra password would,however increase the difficulty of use both to you, and to your intended recipient. Because there is no discernible benefit to the user to add multiple password shields, we simply do not allow the option.

- **Does ShieldMyfiles store my password for this Shield?**

    ShieldMyfiles stores a "hash" of your password, which allows us to check future password entry attempts. We never store passwords in-the-clear. If you elect to make your password non-resettable (see discussion above), your password is not only hashed, but the hash is then ciphered with a secret that is stored as part of the shielded file, adding another layer of security to the way we store your password.

        Why even store the hash? Wouldn't it be better if nothing were stored at all?

        Shield inputs, like a password, are required by ShieldMyfiles to operate with reasonable assurance that authorized individuals are attempting to access Shielded files, and that the Shield criteria that they provide is in fact correct.

    **This shield information is entirely separate from your data which the Shields protect.  ShieldMyfiles never uploads, processes, stores, reads, transmits, or receives the data you protect.**

---

## Step-By-Step Guide to the Password Shield

1. In the section titled "Shields" click on the button called "Password".
2. The Password Shield Form will open below.
3. Type the password you wish to use in the Password Shield Form.
4. Confirm the password, in the confirm Password Shield Form.
5. You can check the "I don't want the password to be resettable (most secure)" box, or not. See here, for an explanation of this option.
6. When you have entered a password and confirmed it, the button which reads "Set Shield" will be active. Click it.
7. Your Password Shield is now set. In the Shields section the Password button should now read, "Edit Password" and the icon should  be Green.

![alt text](../images/ShieldPassForm.png)

---

## Step-by-Step guide to Editing the Password Shield

1. In the section titled "Shields" click on the button called "Edit Password".
2. The Password Shield Form will open below.
3. Repeat the steps found above, in Step-By-Step Guide to the Password Shield

![alt text](../images/EditPass.png)
