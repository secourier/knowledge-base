# Pick Shields

*Picking Shields is how you select the specific way you want to protect your file or files using the ShieldMyfiles app.*

## FAQ

- **Can I set more than one Shield?**

    Absolutely! In fact, ShieldMyfiles is entirely unique in its ability to provide multiple protective Shields.

    It is a core tenant of ShieldMyfiles that users should be able to scale their security to their level of need.  By having the option to add multiple shields to a single file you can enhance your security to match your risk. For example, you can use a password to provide solid security, but adding a password and an expiration date together guarantees that the sensitive files are addressed securely AND in a timely manner.
- **Why can't I set more than one of the _same type of Shield_?**

    The simple answer is that multiple iterations of the same shield do not enhance your security, but they greatly increase the difficulty of accessing your files.

    The Shields we use to protect your files are separate and distinct from the cryptographic pad we use to encipher your data. As such, adding shields does not increase key strength, or provide increased randomness. Because there is no discernible benefit to the user to add multiples of the same type of shield, we simply do not allow the option.
- **What Shield information does ShieldMyfiles store?**

    ShieldMyfiles stores all Shield information. Shield inputs are required by ShieldMyfiles to operate with reasonable assurance that authorized individuals are attempting to access Shielded files, and that the Shield criteria that they provide is in fact correct.

    This shield information is entirely separate from your data which the Shields protect.  ShieldMyfiles never uploads, processes, stores, reads, transmits, or receives the data you protect.

---

## Pick the Shield, or Shields you Like

- [Password Shield](./Password-Shield.md)
