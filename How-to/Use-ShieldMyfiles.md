# How to Use ShieldMyfiles

[Opening Shielded Files](./Opening-Shielded-Files.md)

[Expiration Date Shield](./Expiration-Date-Shield.md)

[Password Shield](./Password-Shield.md)

[Pick Shields](./Pick-Shields.md)

[Shielding Files](./Shielding-Files.md)

[Pick Files](./Pick-Files.md)
