# Opening Shielded Files

*This guide will walk you through the process of opening files.*

## Step-by-step guide

1. You need to select the file you wish to open off your hard drive.
    - If the file was emailed to you as an attachment, you must first download the .html file, and select it from your download folder.
    - If the file was shared with you via a file sharing service (like OneDrive or Dropbox) you must select the file from your sync folder, or you must download the file to your computer, and then select it from the folder where you saved it.
    - If the file is on your computer, or a thumb drive, you can select the file from the folder where it is saved.
2. Now that the file is selected, you will see the "Complete Shields" section which contains the shields that need to completed. Work through each shield (enter passwords, SMS codes etc.) until you see a green circle with a check mark in it, indicating that the shield is complete and verified.
3. Once you have completed each shield, you will see the decipher button appear. Click it.
4. After clicking the decipher button, your file will be decipher, and automatically downloaded to your machine. It will be saved in the folder your have designated for downloads with your browser.
