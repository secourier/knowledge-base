# Expiration Date Shield

## FAQ

- **Why is the Expiration Date Shield automatically added to every shielded file?**

    Having shielded files expire dramatically increases their security. On a purely theoretical level, all forms of file security will be breached on a long enough timeline.  By expiring files, ShieldMyfiles eliminates the possibility that a long-forgotten file could eventually be accessed by a bad actor.

- **Why can't I remove the Expiration Date shield?**

    Our commitment to providing our users with the highest levels of security is predicated upon building reasonable assurances that our system does not unnecessarily expose our users to known risks.  Expiring shielded files, is one of the easiest mitigations to known security risks; furthermore, it is simple for users to use as there is no information to remember (password) or device to possess (a phone for SMS messages). As such, we require it on every transaction.

- **Is the Expiration Date I set used as the encryption key?**

    No.

    ShieldMyfiles uses [One-Time-Pad](./Shielding-Files.md) (OTP) ciphers to shield files. OTP operates by issuing a pad (or key) that is equal in size to the file you are protecting, and then combining your file with the corresponding character in the pad. The Expiration Date you set is used by ShieldMyfiles as a mechanism by which we authorize release of the pad to your intended recipient.

- **Why is one-year from now, the longest date I can set for expiration?**

    When a file expires with ShieldMyfiles, it is burned for ever.  The file is irretrievable at that point. Our hope is to set expiration dates that are no so far into the future that a user forgets the time horizon and one days finds that file is permanently inaccessible. We encourage our users to set any expiration time horizon they wish, but for periods of time that are longer than one calendar year, we require that the file have the Expiration Shield re-applied to continue it beyond the one-year mark.

- **Does ShieldMyfiles store my expiration date for this Shield?**

    ShieldMyfiles stores a "hash" of your selected date, which allows us to check future access attempts.

    **Why even store the hash? Wouldn't it be better if nothing were stored at all?**

        Shield inputs, are required by ShieldMyfiles to operate with reasonable assurance that authorized individuals are attempting to access Shielded files, and that the Shield criteria that they provide is in fact correct.

    **This shield information is entirely separate from your data which the Shields protect.  ShieldMyfiles never uploads, processes, stores, reads, transmits, or receives the data you protect.**

---

### Step-by-step guide

Coming Soon!
