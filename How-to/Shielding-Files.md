# Shielding Files

Welcome to ShieldMyfiles. You are using a state-of-the-art file protection utility. We are here to help you keep your files safe with protection you control.

---

## Why Shield Files?

*Simply put, its all about the data.*

Any time information is lost, stolen, or compromised the concern is with **_the data_**. Every social security number, credit card number, banking record, or patient health record starts life as a file, or part of a file.  By protecting individual files, ShieldMyfiles is providing protection at the core; so even if network firewalls are compromised, individual machines are lost or breached, or credentials are stolen - the data (the files) remain enciphered and unreadable.

## One-Time-Pad (OTP) vs. Encryption

- **OTP** is a cryptographic technique where plaintext (your data) is combined with a random key (the 'One-Time-Pad'). Key features of OTP are:
    - The Pad is at least as long as the data that must be enciphered.
    - o	Each Pad is used only once.
    - The Pad is not the Shield, meaning the Shield input you set is separate and distinct from the Pad.
- **Encryption** is the process of encoding of data such that only authorized parties can read it. There are two broad categories of common encryption used today.
    1. *Symmetric* - where the same key is used for encryption and decryption
    2. *Asymmetric* - where one key is used for encryption (frequently referred to as a public key) and a separate key is used for decryption (frequently referred to as a private key).
- Under either schema, encryption keys are not as long as the data to be protected.
- Keys are frequently reused.

---

## The ShieldMyfiles Big Three

1. **Protection without possession**
    - At ShieldMyfiles we do not believe you can keep a secret by telling it. By that logic, at ShieldMyfiles you never have to give us access to your data **_EVER_**, in order for us to protect it. As such, you get protection without *our* possession.
2. **Customizable protection you control**
    - Shields give you a choice in how you want to protect your data. For example, use a password shield or an expiration date, or both! Shields put you in charge of how to protect your data, so you can scale your security with your level of need.
3. **Unique ciphering that doesn't rely on encryption key exchange or accounts**
    - Shielded files are enciphered using a one-time-pad, rendering them unreadable and unusable. This high level of security is provided without relying on complex encryption key exchanges, or our users having to manage a mess of user accounts!

## What Will Shields Guard Against?

- As mentioned before, Shields protect individual files. Because protections are being applied at the individual file level several truisms can be understood:
    1. attached to an **email**,
    2. uploaded to the **cloud**,
    3. on a **thumb-drive**, or
    4. on your favorite **collaboration platform**.

---

![alt text](../images/shielding.png)
