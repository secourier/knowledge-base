# Pick Files

*Picking Files is how you select the file or files you want to shield using the ShieldMyfiles App.*

## FAQ

- **Am I uploading my file(s) to ShieldMyfiles when I pick them?**

    No. ShieldMyfiles maintains as one of its core operating principles that your data and files always remain on your system(s). Your files, whether Shielded or in-the-clear, are never uploaded to our servers, or transmitted through our systems.

    - At ShieldMyfiles you get protection without our having possession.

- **How does ShieldMyfiles do all this work on my files without uploading them to ShieldMyfiles servers?**

    ShieldMyfiles is built to take advantage of the powerful features found in modern web browsers, including javascript and HTML5.  One of those features includes the ability to process local files in your browser.  ShieldMyfiles does this to ensure that you, and only you, maintain control over your files.

- **What is the max file size I can Shield?**

    100mb.

- **Why is there a file size limit?**

    Unlike your computer's operating system, browsers have limits on the size of the files they are able to process.  ShieldMyfiles limits file size to 100mb because it is well within most browsers' capabilities, and rarely produces file processing errors.

- **Are there any file type limitations?**

    No. ShieldMyfiles is entirely File Type agnostic.

- **If I pick multiple files how are they combined?**

    ShieldMyfiles combines multiple files into a single file for shielding by zipping them. This occurs on your system, in the browser.  The process is the same as creating a zip archive on your computer. The zipped file is then ciphered in your browser.

---

## Step-by-step guide to Picking Files

1. In the section titled "Files" click on the button called "Pick Files".
2. Your browser will open its file explorer window.
3. Using the file explorer window, select the file or file(s) you want to Shield.
4. The file(s) you have selected will appear below the button which now reads "Change Files", and the icon will be green.
5. You are ready to Pick Shields!


### Pick Files Button
![alt text](../images/FilePicketButton.png)

### Files Added View
![alt text](../images/FilesAdded.png)

---

## Step-by-Step guide to Changing Picked Files

1. In the section titled "Files" click on the button called "Change Files".
2. Your browser will open its file explorer window.
3. Using the file explorer window, select the new file or file(s) you want to Shield.
4. The file(s) you have selected will appear below the button which again reads "Change Files", and the icon will be green.
5. You are ready to Pick Shields!

### Change Files Button
![alt text](../images/CHangeFiles.png)

### Files Added View
![alt text](../images/FilesAdded.png)

---
