# ShieldMyfiles Knowledge Base

Welcome to ShieldMyfiles Knowledge Base. Here you will find Frequently Asked Questions (FAQs) and Step-by-Step instruction for using *ShieldMyfiles*.

---

## How to Use ShieldMyfiles

[Opening Shielded Files](./Opening-Shielded-Files.md)

[Expiration Date Shield](./Expiration-Date-Shield.md)

[Password Shield](./Password-Shield.md)

[Pick Shields](./Pick-Shields.md)

[Shielding Files](./Shielding-Files.md)

[Pick Files](./Pick-Files.md)

---